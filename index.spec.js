const sinon = require('sinon')
const { expect } = require('chai')
const FirestoreClass = require('@google-cloud/firestore')

const { handler } = require('./')

const res = { send: sinon.stub(), set: sinon.stub(), status: sinon.stub() }
const mockFirestoreAdd = sinon.stub().resolvesArg(0)

class MockFirestore {
  constructor() {}

  collection = sinon.stub().returnsThis()
  add = mockFirestoreAdd
}

const mockFirestore = new MockFirestore()
const artistPreregistration = handler(mockFirestore)

afterEach(() => {
  sinon.resetHistory()
})

it('should send one and only one response', () => {
  const req = {}

  artistPreregistration(req, res)

  expect(res.send.calledOnce).to.be.true
})

it('should enforce CORS origin', () => {
  const req = {}

  artistPreregistration(req, res)

  expect(
    res.set.calledOnceWith(
      'Access-Control-Allow-Origin',
      process.env.CORS_ORIGIN || 'localhost'
    )
  ).to.be.true
  expect(res.send.calledOnce).to.be.true
})

it('should allow valid POST requests', async () => {
  const req = { method: 'POST', body: { email: 'test@example.com' } }

  await artistPreregistration(req, res)

  expect(
    res.status.calledOnceWith(201)
  ).to.be.true
  expect(res.send.calledOnce).to.be.true
})

it('should support OPTIONS preflight requests', () => {
  const req = { method: 'OPTIONS' }

  artistPreregistration(req, res)

  expect(
    res.set.calledWith('Access-Control-Allow-Methods', 'POST')
  ).to.be.true
  expect(
    res.set.calledWith('Access-Control-Allow-Headers', 'Content-Type')
  ).to.be.true
  expect(res.status.calledOnceWith(204)).to.be.true
  expect(res.send.calledOnce).to.be.true
})

it('should deny all other HTTP methods', () => {
  const req = { method: 'GET' }

  artistPreregistration(req, res)

  expect(res.status.calledOnceWith(405)).to.be.true
  expect(res.send.calledOnce).to.be.true
})

describe('should require email address in the request body', () => {
  it('returns 400 when email missing', async () => {
    const req = { method: 'POST', body: {} }

    await artistPreregistration(req, res)

    expect(res.status.calledOnceWith(400)).to.be.true
    expect(res.send.calledOnce).to.be.true
  })

  it('returns 201 when email present', async () => {
    const reqWithEmail = { method: 'POST', body: { email: 'text@example.com' }}

    await artistPreregistration(reqWithEmail, res)

    expect(res.status.calledOnceWith(201)).to.be.true
    expect(res.send.calledOnce).to.be.true
  })
})

it('should timestamp each submission', async () => {
  const req = { method: 'POST', body: {
    email: 'test@example.com',
  } }

  await artistPreregistration(req, res)

  expect(res.status.calledOnceWith(201)).to.be.true
  expect(res.send.calledOnce).to.be.true
  expect(mockFirestore.add.calledOnceWith(
    sinon.match.has('created')
  )).to.be.true
})

it('should sanitize and store user input', async () => {
  const req = { method: 'POST', body: {
    email: 'test@example.com',
    invalidProperty: ':)',
    name: 'test'
  } }

  await artistPreregistration(req, res)

  expect(res.status.calledOnceWith(201)).to.be.true
  expect(res.send.calledOnce).to.be.true
  expect(mockFirestore.collection.calledOnceWith(
    'artist-preregistration'
  )).to.be.true
  expect(mockFirestore.add.calledOnceWith(
    sinon.match(value => {
      return !!value.email && !!value.name && !value.invalidProperty
    })
  )).to.be.true
})

it('should return 500 on firestore error', async () => {
  const req = { method: 'POST', body: { email: 'test@example.com' } }

  mockFirestore.add = sinon.stub().rejects()

  await artistPreregistration(req, res)

  expect(res.status.calledOnceWith(500)).to.be.true
  expect(res.send.calledOnce).to.be.true

  mockFirestore.add = mockFirestoreAdd
})

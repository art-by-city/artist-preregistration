gcloud functions deploy artist-preregistration \
  --entry-point=artistPreregistration \
  --trigger-http \
  --runtime=nodejs16 \
  --allow-unauthenticated \
  --project art-by-city-staging-299519 \
  --set-env-vars GCP_PROJECT=art-by-city-staging-299519,CORS_ORIGIN=https://staging.artby.city

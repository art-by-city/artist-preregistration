gcloud functions deploy artist-preregistration \
  --entry-point=artistPreregistration \
  --trigger-http \
  --runtime=nodejs16 \
  --allow-unauthenticated \
  --project clean-linker-273919 \
  --set-env-vars GCP_PROJECT=clean-linker-273919,CORS_ORIGIN=https://artby.city

const { expect } = require('chai')
const { request } = require('gaxios')

const ORIGIN = 'localhost'
const PORT = process.env.PORT || 8080
const BASE_URL = `http://${ORIGIN}:${PORT}`
const FUNCTION_NAME = 'artistPreregistration'

it('should accept the minimum submission', async () => {
  const submission = { email: 'test@example.com' }

  const response = await request({
    url: `${BASE_URL}/${FUNCTION_NAME}`,
    method: 'POST',
    data: submission
  })

  expect(response.status).to.equal(201)
})

it('should respond to OPTIONS requests', async () => {
  const response = await request({
    url: `${BASE_URL}/${FUNCTION_NAME}`,
    method: 'OPTIONS'
  })

  expect(response.status).to.equal(204)
  expect(
    response.headers['access-control-allow-headers']
  ).to.equal('Content-Type')
  expect(response.headers['access-control-allow-methods']).to.equal('POST')
  expect(response.headers['access-control-allow-origin']).to.equal(ORIGIN)
})

it('should return 405 on all other HTTP methods', async () => {
  try {
    await request({
      url: `${BASE_URL}/${FUNCTION_NAME}`,
      method: 'GET'
    })
  } catch ({ response }) {
    expect(response.status).to.equal(405)
  }
})

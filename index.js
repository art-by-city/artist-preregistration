const Firestore = require('@google-cloud/firestore')

const isProduction = process.env.NODE_ENV === 'production'
                  || process.env.NODE_ENV === 'staging'

const GCP_PROJECT = process.env.GCP_PROJECT || 'art-by-city-dev'
const COLLECTION_NAME = 'artist-preregistration'
const CORS_ORIGIN = isProduction ? process.env.CORS_ORIGIN : '*'
const DB_HOST = process.env.DB_HOST || 'localhost'
const DB_PORT = Number.parseInt(process.env.DB_PORT || '8080')
const DB_SSL = process.env.DB_SSL === 'true'

const ACCEPTED_FIELDS = [
  'email',
  'name',
  'arweaveAddress',
  'discordHandle',
  'location',
  'artistType',
  'artistMedium',
  'artistLinks',
  'preferredUsername',
  'howDidYouHearAboutUs',
  'referral'
]

const firestoreOpts = { projectId: GCP_PROJECT }

if (!isProduction) {
  firestoreOpts.host = DB_HOST
  firestoreOpts.port = DB_PORT
  firestoreOpts.ssl = DB_SSL
}

const firestore = new Firestore(firestoreOpts)

const createHandler = (firestore) => (req, res) => {
  res.set('Access-Control-Allow-Origin', CORS_ORIGIN)

  switch (req.method) {
    case 'POST':
      if (!req.body.email) {
        res.status(400)
      } else {
        const created = Firestore.Timestamp.fromDate(new Date())
        const data = req.body || {}
        const sanitized = {}
        for (const prop in data) {
          if (data.hasOwnProperty(prop) && ACCEPTED_FIELDS.includes(prop)) {
            sanitized[prop] = data[prop]
          }
        }

        return firestore.collection(COLLECTION_NAME).add({
          ...sanitized,
          created
        }).then(doc => {
          res.status(201)

          return res.send()
        }).catch(err => {
          console.error(err, 'Request Body:', req.body)
          res.status(500)

          return res.send({
            error: 'Unexpected error while saving your response'
          })
        })
      }
      break
    case 'OPTIONS':
      res.set('Access-Control-Allow-Methods', 'POST')
      res.set('Access-Control-Allow-Headers', 'Content-Type')
      res.status(204)
      break
    default:
      res.status(405)
      break
  }

  res.send()
}

exports.handler = createHandler

exports.artistPreregistration = createHandler(firestore)
